<?php

/* Portfolio class with required functions */

require_once('../includes/database_connection.php');
require_once('stock.php');

class Portfolio {
  
  public  $id;
  private $user_id;
  private $cash_balance;
  private $close_connection = false;
  private $errors = [];
  private $market_value;
  
  //~ find portfolio for a certain user (by user_id)
  
  public static function find_by_user_id($user_id, $dbh = null) {
    $close_connection = false;
    //~ if $dbh does not exist, try to connect
    if (!$dbh) {
      $close_connection = true;
      $dbh = connect();
    }
    //~ if connection failed, return null
    if (!$dbh) { return false; }
    
    $stmt = $dbh->prepare("SELECT * FROM `portfolios` WHERE user_id = :user_id");
    $stmt->bindValue(':user_id', $user_id, PDO::PARAM_INT);
    
    $portfolio = null;
    //~ if execution suceeds
    if ($stmt->execute()) {
      $portfolio = new Portfolio($stmt->fetch());
    }
    
    //~ reset $dbh
    if ($close_connection) {
      $dbh = null;
    }
    
    return $portfolio;
  }
  
  //~ class constructor
  
  public function __construct($attributes = []) {
    $allowed_attributes = ['id','user_id','cash_balance'];
    foreach ($attributes as $key => $value) {
      if (in_array($key, $allowed_attributes)) {
        $this->$key = $value;
      }
    }
  }

  //~ save Portfolio entry 

  public function save($dbh = null) {
    //~ if $dbh does not exist, try to connect
    if (!$dbh) {
      $this->close_connection = true;
      $dbh = connect();
      $dbh->beginTransaction();
    }
    //~ if connection failed, return null
    if (!$dbh) { return false; }

    $stmt = null;
    //~ if portfolio does not yet exist
    if (empty($this->id)) {
      $sql = "INSERT INTO `portfolios` (user_id, cash_balance) VALUES (:user_id, :cash_balance)";

      $stmt = $dbh->prepare($sql);
      $stmt->bindValue(':user_id', $this->user_id, PDO::PARAM_INT);
      $stmt->bindValue(':cash_balance', $this->cash_balance, PDO::PARAM_STR);

    //~ if portfolio already exists
    } else {
      $sql = "UPDATE `portfolios` SET cash_balance = :cash_balance WHERE id = :id";

      $stmt = $dbh->prepare($sql);
      $stmt->bindValue(':id', $this->id, PDO::PARAM_INT);
      $stmt->bindValue(':cash_balance', $this->cash_balance, PDO::PARAM_STR);
    }
    //~ if execution succeds
    if ($stmt->execute()) {
      //~ find portfolio_id if portfolio is new
      if (empty($this->id)) {
        $this->id = Portfolio::find_by_user_id($this->user_id, $dbh)->id;
      }
      //~ reset $dbh
      if ($this->close_connection) {
        $dbh->commit();
        $dbh = null;
      }
      return true;
    //~ if execution fails
    } else {
      if ($this->close_connection) {
        $dbh->rollBack();
        $dbh = null;
      }
      echo $stmt->errorInfo()[2];
      return false;
    }
  }

  //~ return errors

  public function errors() {
    return empty($this->errors) ? null : $this->errors;
  }
  
  // return the current cash balance
  public function cash_balance() {
    return "$" . number_format($this->cash_balance, 2);
  }
  
  //~ sum up market value for stocks
  
  public function market_value() {
    if (!isset($this->market_value)) {
      $sum = 0;
      foreach ($this->stocks() as $stock) {
        $sum += $stock->market_value();
      }
      $this->market_value = $sum;
    }
    return "$" . number_format($this->market_value, 2);
  }
  
  //~ determine total value (including cash)
  
  public function total_value() {
    if (!isset($this->market_value)) {
      $this->market_value();
    }
    return "$" . number_format($this->cash_balance + $this->market_value, 2);
  }
  
  // returns true if there are no stocks in the portfolio
  
  public function is_empty() {
    return Stock::count_by_portfolio_id($this->id) == 0;
  }
  
  //~ find stocks in a portfolio
  
  public function stocks() {
    return Stock::find_all_by_portfolio_id($this->id);
  }
  
  //~ buy stocks
  
  public function buy($collection) {
    // check if we have enough cash to buy selection
    if ($this->insufficient_cash_balance($collection)) {
      $this->errors[] = 'Insufficient cash balance, to complete transaction';
      return false;
    }
    
    // check if number is negative
    if ($this->negative_amount($collection)) {
      $this->errors[] = 'Sorry, negative numbers are not supported';
      return false;
    }

    // add selection of stocks to portfolio
    if ($dbh = connect()) {
      $dbh->beginTransaction();
      
      foreach ($collection as $symbol => $values) {
        $amount = intval($values['amount']);
        $price  = doubleval($values['price']);
        $cost   = $amount * $price;
     
        // update cash balance
        $this->cash_balance = $this->cash_balance - $cost;
     
        // lookup existing stock or create new one
        $stock = null;
        if ($stock = Stock::find_by_protfolio_id_and_symbol($this->id, $symbol, $dbh)) {
          $stock->add($amount, $cost);
        } else {
          $stock = new Stock(array(
            'portfolio_id' => $this->id,
            'symbol'       => $symbol,
            'quantity'     => $amount,
            'cost'         => $cost
          ));
        }
        //~ if save stock failed
        if (!$stock->save($dbh)) {
          $dbh->rollBack();
          $dbh = null;
          
          return false;
        }
      }
      //~ if save portfolio failed
      if (!$this->save($dbh)) {
        $dbh->rollBack();
        $dbh = null;
        
        return false;
      }
      
      $dbh->commit();
      $dbh = null;
    //~ id database connection failed 
    } else {
      $this->errors[] = 'Failed to establish database connection, please try again later';
      return false;
    }
    
    return true;
  }
  
  //~ sell stocks
  
  public function sell($collection) {
    //~ check if input number exceeds number of stocks in portfolio
    if ($this->insufficient_stocks($collection)) {
      $this->errors[] = "You don't have that many stocks to sell";
      return false;
    }
    //~ check if number is negative
    if ($this->negative_amount($collection)) {
      $this->errors[] = 'Sorry, negative numbers are not supported';
      return false;
    }

    // remove selection of stocks from our portfolio
    if ($dbh = connect()) {
      $dbh->beginTransaction();
      
      foreach ($collection as $symbol => $values) {
        $amount = intval($values['amount']);

        $stock = Stock::find_by_protfolio_id_and_symbol($this->id, $symbol);
        //~ if stock can't be found
        if (!$stock) {
          $this->errors[] = "Failed to lookup: $symbol";
          $dbh->rollBack();
          $dbh = null;
          
          return false;
        }
        //~ if the number of stocks is too small
        if ($stock->quantity < $amount) {
          $this->errors[] = "Insufficient stocks available";
          $dbh->rollBack();
          $dbh = null;
          
          return false;
        }
     
        // update cash balance from sale
        $balance = $stock->market_value_per_share() * $amount;
        $this->cash_balance = $this->cash_balance + $balance;
     
        // update stock quantity or destroy
        if ($stock->quantity > $amount) {
          $stock->remove($amount, $balance);
        } else {
          //~ if destroy didn't work
          if (!$stock->destroy($dbh)) {
            $this->errors[] = "Failed to remove: '$symbol' from portfolio";
            $dbh->rollBack();
            $dbh = null;
            
            return false;
          }
        }
        //~ if stock update failed
        if (!$stock->save($dbh)) {
          $this->errors[] = "Failed to update quantity for: $symbol";
          $dbh->rollBack();
          $dbh = null;
          
          return false;
        }
      }
      //~ if portfolio update failed
      if (!$this->save($dbh)) {
        $dbh->rollBack();
        $dbh = null;
        
        return false;
      }
      
      $dbh->commit();
      $dbh = null;
    //~ if database connection failed
    } else {
      $this->errors[] = 'Failed to establish database connection, please try again later';
      return false;
    }
    
    return true;
  }
  
  //~ determine if amount is negative
  
  private function negative_amount($collection) {
    foreach ($collection as $key => $values) {
      $amount = intval($values['amount']);
      if ($amount < 0) {
        return true;
      }
    }
    
    return false;
  }

  //~ check cash balance

  private function insufficient_cash_balance($collection) {
    $sum = 0;
    foreach ($collection as $key => $values) {
      $amount = intval($values['amount']);
      $price  = doubleval($values['price']);
      
      if ($amount > 0) {
        $sum += $amount * $price;
      }
    }
    
    return $sum > $this->cash_balance;
  }
  
  //~ check if stocks are sufficient
  
  private function insufficient_stocks($collection) {
    foreach ($collection as $symbol => $values) {
      if ($values['amount'] != '' && ($stock = Stock::find_by_protfolio_id_and_symbol($this->id, $symbol))) {
        if ($stock->quantity < intval($values['amount'])) {
          return true;
        }
      }
    }
    return false;
  }
}

?>
