<?php

/* Stock class with required functions */

require_once('../includes/database_connection.php');

class Stock {

  public $id;
  private $portfolio_id;
  public $symbol;
  public $quantity;
  public $cost;
  private $close_connection;
  
  //~ find stock by portfolio id and symbol
  
  public static function find_by_protfolio_id_and_symbol($portfolio_id, $symbol, $dbh = null) {
    $close_connection = false;
    //~ if $dbh does not exist, try to connect
    if (!$dbh) {
      $dbh = connect();
      $close_connection = true;
    }
    //~ if connection failed, return null
    if (!$dbh) { return false; }
    
    $stmt = $dbh->prepare("SELECT * FROM `stocks` WHERE portfolio_id = :portfolio_id AND symbol = :symbol");
    $stmt->bindValue(':portfolio_id', $portfolio_id, PDO::PARAM_INT);
    $stmt->bindValue(':symbol',       $symbol,       PDO::PARAM_STR);
    
    $stock = null;
    if ($stmt->execute()) {
      if ($attributes = $stmt->fetch()) {
        $stock = new Stock($attributes);
      }
    }
    
    //~ reset $dbh
    if ($close_connection) {
      $dbh = null;
    }
    
    return $stock;
  }
  
  //~ count number of stocks by portfolio id
  
  public static function count_by_portfolio_id($portfolio_id) {
    $count = 0;
    if ($dbh = connect()) {
      $stmt = $dbh->prepare("SELECT COUNT(*) AS number_of_stocks FROM `stocks` WHERE portfolio_id = :portfolio_id");
      $stmt->bindValue(':portfolio_id', $portfolio_id, PDO::PARAM_INT);
      
      if ($stmt->execute()) {
        $count = intval($stmt->fetch()['number_of_stocks']);
      }
      $dbh = null;
    }
    
    return $count;
  }
  
  //~ find all stocks, that belong to a certain portfolio
  
  public static function find_all_by_portfolio_id($portfolio_id) {
    $stocks = [];
    if ($dbh = connect()) {
      $stmt = $dbh->prepare("SELECT * FROM `stocks` WHERE portfolio_id = :portfolio_id");
      $stmt->bindValue(':portfolio_id', $portfolio_id, PDO::PARAM_INT);
      
      if ($stmt->execute()) {
        while ($attributes = $stmt->fetch()) {
          $stocks[] = new Stock($attributes);
        }
      }
    }
    return $stocks;
  }
  
  //~ class constructor
  
  public function __construct($attributes = []) {
    $allowed_attributes = ['id','portfolio_id','symbol','quantity','cost'];
    foreach ($attributes as $key => $value) {
      if (in_array($key, $allowed_attributes)) {
        $this->$key = $value;
      }
    }
  }

  //~ save Stock entry 
  
  public function save($dbh = null) {
    //~ if $dbh does not exist, try to connect
    if (!$dbh) {
      $this->close_connection = true;
      $dbh = connect();
      $dbh->beginTransaction();
    }
    //~ if connection failed, return null
    if (!$dbh) { return false; }

    $stmt = null;
    //~ if stock does not yet exist
    if (empty($this->id)) {
      $sql = "INSERT INTO `stocks` (portfolio_id, symbol, quantity, cost) VALUES (:portfolio_id, :symbol, :quantity, :cost)";

      $stmt = $dbh->prepare($sql);
      $stmt->bindValue(':portfolio_id', $this->portfolio_id, PDO::PARAM_INT);
      $stmt->bindValue(':symbol',       $this->symbol,       PDO::PARAM_STR);
      $stmt->bindValue(':quantity',     $this->quantity,     PDO::PARAM_INT);
      $stmt->bindValue(':cost',         $this->cost,         PDO::PARAM_STR);
    
    //~ if stock already exists
    } else {
      $sql = "UPDATE `stocks` SET quantity = :quantity, cost = :cost WHERE id = :id";

      $stmt = $dbh->prepare($sql);
      $stmt->bindValue(':id',           $this->id,           PDO::PARAM_INT);
      $stmt->bindValue(':quantity',     $this->quantity,     PDO::PARAM_INT);
      $stmt->bindValue(':cost',         $this->cost,         PDO::PARAM_STR);
    }

    //~ if execution succeds
    if ($stmt->execute()) {
      if ($this->close_connection) {
        $dbh->commit();
        $dbh = null;
      }
      return true;
    
    //~ if execution fails
    } else {
      if ($this->close_connection) {
        $dbh->rollBack();
        $dbh = null;
      }
      echo $stmt->errorInfo()[2];
      return false;
    }
  }

  //~ delete stock
  
  public function destroy($dbh = null) {
    //~ if $dbh does not exist, try to connect
    if (!$dbh) {
      $this->close_connection = true;
      $dbh = connect();
      $dbh->beginTransaction();
    }
    //~ if connection failed, return null
    if (!$dbh) { return false; }

    $stmt = $dbh->prepare("DELETE FROM `stocks` WHERE id = :id");
    $stmt->bindValue(':id', $this->id, PDO::PARAM_INT);

    //~ if execution succeds
    if ($stmt->execute()) {
      if ($this->close_connection) {
        $dbh->commit();
        $dbh = null;
      }
      return true;
    
    //~ if execution fails
    } else {
      if ($this->close_connection) {
        $dbh->rollBack();
        $dbh = null;
      }
      echo $stmt->errorInfo()[2];
      return false;
    }
  }

  //~ update quantity and cost if stocks are added
  
  public function add($amount, $cost) {
    $this->quantity = $this->quantity + intval($amount);
    $this->cost     = $this->cost     + doubleval($cost);
  }

  //~ update quantity and cost if stocks are removed
  
  public function remove($amount, $balance) {
    $this->quantity = $this->quantity - intval($amount);
    $this->cost     = $this->cost     - doubleval($balance);
  }
  
  //~ Additional helper function to get quantitative information for stocks
  
  public function average_per_share() {
    return $this->cost / $this->quantity;
  }
  
  public function market_value() {
    $this->update_market_value();
    if (isset($this->current_market_values)) {
      return doubleval($this->current_market_values[1]) * $this->quantity;
    }
    return 0;
  }
  
  public function market_value_per_share() {
    $this->update_market_value();
    if (isset($this->current_market_values)) {
      return doubleval($this->current_market_values[1]);
    }
    return -1;
  }
  
  public function market_change() {
    $this->update_market_value();
    if (isset($this->current_market_values)) {
      return $this->current_market_values[4];
    }
  }
  
  public function market_percent_change() {
    $this->update_market_value();
    if (isset($this->current_market_values)) {
      return 100 * $this->market_change() / $this->current_market_values[1];
    }
  }
  
  private function update_market_value() {
    if (!isset($this->current_market_values)) {
      $url = "http://download.finance.yahoo.com/d/quotes.csv?s={$this->symbol}&f=sl1d1t1c1ohgv&e=.csv";
      if ($handle = fopen($url, 'r')) {
        $this->current_market_values = fgetcsv($handle);
        fclose($handle);
      }
    }
  }
}

?>
