<?php

/* User class with required functions */

require_once('../includes/database_connection.php');
require_once('portfolio.php');

class User {
  
  public $id;
  public $email;
  private $password;
  private $password_confirmation;
  private $password_hash;
  private $password_salt;
  private $errors = array();
  
  private $portfolio;
  
  //~ find user by id
  
  public static function find($id, $dbh = null) {
    $sql = "SELECT * FROM `users` WHERE id = :id";

    return User::find_with($sql, array(':id' => $id), $dbh);
  }
  
  //~ find user by e-mail
  
  public static function find_by_email($email, $dbh = null) {
    $sql = "SELECT * FROM `users` WHERE email = :email";
    
    return User::find_with($sql, array(':email' => $email), $dbh);
  }
  
  //~ common user find function
  
  private static function find_with($sql, $attributes, $dbh = null) {
    $close_connection = false;
    //~ if $dbh does not exist, try to connect
    if (!$dbh) {
      $close_connection = true;
      $dbh = connect();
    }
    //~ if connection failed, return null
    if (!$dbh) {
      return null;
    }
    
    $stmt = $dbh->prepare($sql);
    foreach ($attributes as $key => $value) {
      $stmt->bindValue($key, $value, PDO::PARAM_STR);
    }
    $user = null;
    //~ get user
    if ($stmt->execute()) {
      if ($attributes = $stmt->fetch()) {
        $user = new User($attributes);
      }
    }
    //~ reset $dbh
    if ($close_connection) {
      $dbh = null;
    }
    return $user;
  }
  
  //~  class constructor
  
  public function __construct($attributes = []) {
    $allowed_attributes = ['id','email','password','password_confirmation','password_hash','password_salt'];
    foreach ($attributes as $key => $value) {
      if (in_array($key, $allowed_attributes)) {
        $this->$key = $value;
      }
    }
  }
    
  //~ save User
   
  public function save() {
    //~ check e-mail and password
    if (!$this->valid()) {
      return false;
    }
    
    //~ encrypt password
    $password_salt = hash("SHA1", microtime());
    $password_hash = hash("SHA1", $this->password . $password_salt);
    
    if ($dbh = connect()) {
      $sql = "INSERT INTO `users` (email, password_hash, password_salt) VALUES (:email, :password_hash, :password_salt)";
      
      //~ lock user table
      $dbh->beginTransaction();
      $stmt = $dbh->prepare($sql);
      $stmt->bindValue(':email', $this->email, PDO::PARAM_STR);
      $stmt->bindValue(':password_hash', $password_hash, PDO::PARAM_STR);
      $stmt->bindValue(':password_salt', $password_salt, PDO::PARAM_STR);
      //~ if execution succeds
      if ($stmt->execute()) {
        //~ find user id if user is new and create portfolio
        if (empty($this->id)) {
          $this->id = User::find_by_email($this->email, $dbh)->id;
          if ($this->create_portfolio($dbh)) {
            $dbh->commit();
          } else {
            $dbh->rollBack();
            return false;
          }
        } else {
          $dbh->commit();
        }
        
        $dbh = null;
        return true;
      //~ if execution fails
      } else {
        echo $stmt->errorInfo()[2];
        
        $dbh->rollBack();
        $dbh = null;
        return false;
      }
    }
    return false;
  }
  
  //~ validate email and password
  
  public function valid() {
    $this->validate_email();
    $this->validate_password();
    
    return empty($this->errors);
  }
  
  //~ check for errors
  
  public function errors() {
    return empty($this->errors) ? null : $this->errors;
  }
  
  //~ authenticate by checking the encrypted password
  
  public function authenticated($password) {
    $password_hash = hash("SHA1", $password . $this->password_salt);
    return $password_hash === $this->password_hash;
  }
  
  //~ get portfolio for current user
  
  public function portfolio() {
    if (empty($this->portfolio)) {
      $this->portfolio = Portfolio::find_by_user_id($this->id);
    }
    return $this->portfolio;
  }
  
  // Private functions
  
  //~ create portfolio
  
  private function create_portfolio($dbh = null) {
    $portfolio = new Portfolio(array('user_id' => $this->id, 'cash_balance' => 10000));
    return $portfolio->save($dbh);
  }
  
  //~ validate password for new user
  
  private function validate_password() {
    // only validate password when new user
    if (empty($this->id)) {
      //~ check if passwords match
      if (! ($this->password === $this->password_confirmation)) {
        $this->errors['password'][] = 'Passwords mismatch';
      }
      
      //~ check if password has minimum length
      if (strlen($this->password) < 6) {
        $this->errors['password'][] = 'Password must be 6 or more characters';
      }
    }
  }
  
  //~ validate e-mail
  
  private function validate_email() {
    //~ check if e-mail is entered
    if (empty($this->email)) {
      $this->errors['email'][] = 'Email is required';
    }
    //~ check if e-mail looks reasonable
    if (! preg_match('/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/', $this->email)) {
      $this->errors['email'][] = 'Email is invalid';
    }
  }
}

?>
