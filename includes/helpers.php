<?php

/** Renders a template found in the templates folder
  * @param array $data
  */
  
function render($template, $data = array())
{
  $path = __DIR__ . '/../views/' . $template . '.php';
  if (file_exists($path))
  {
    extract($data);
    require($path);
  }
}

//~ check for get request

function is_get_request() {
  return strcmp($_SERVER['REQUEST_METHOD'], 'GET') == 0; 
}

//~ check for post request

function is_post_request() {
  return strcmp($_SERVER['REQUEST_METHOD'], 'POST') == 0; 
}

//~ notifications after redirect

function flash($type, $message = null) {
  if (isset($message)) {
    $_SESSION[$type] = $message;
  } else {
    $message = null;
    if (isset($_SESSION[$type])) {
      $message = $_SESSION[$type];
      unset($_SESSION[$type]);
    }
    
    return $message;
  }
}

function flash_notification() {
  $message = flash('notice');
  if (!empty($message)) {
    return "<div class=\"notice\">{$message}</div>";
  }
}

function flash_error() {
  $message = flash('error');
  if (!empty($message)) {
    return "<div class=\"error\">{$message}</div>";
  }
}

function market_indicator($stock) {
  return $stock->market_percent_change() > 0 ? 'up' : 'down';
}

//~  check if user is logged in

function logged_in() {
  return isset($_SESSION['current_user_id']);
}

//~  set user in $SESSION to current user

function set_current_user($user) {
  session_regenerate_id();
  $_SESSION['current_user_id'] = $user->id;
}

//~ get current user

function current_user() {
  if (logged_in()) {
    $current_user = isset($current_user) ? $current_user : User::find($_SESSION['current_user_id']);
    return $current_user;
  }
}

//~ define naviagation menu

function menus() {
  return [
    'My Portfolio' => '/',
    'Search'       => '/search.php'
  ];
}

//~ Error checking

function has_error($object, $field) {
  return isset($object->errors()[$field]) ? 'field_with_errors' : '';
}

?>
