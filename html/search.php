<!--
controller for search functionality
-->

<?php

session_start();

require_once('../includes/helpers.php');
require_once('../model/user.php');

if (logged_in()) {
  if (isset($_GET['symbol'])) {
    // perform search and render results
    if ($_GET['symbol'] !== '') {
      $symbol = urlencode($_GET['symbol']);
      $url = "http://download.finance.yahoo.com/d/quotes.csv?s={$symbol}&f=sl1d1t1c1ohgv&e=.csv";
      $stocks = [];
      if ($handle = fopen($url, 'r')) {
        while ($stock = fgetcsv($handle)) {
          if ($stock[2] !== 'N/A') {
            $stocks[] = $stock;
          }
        }
        fclose($handle);
      }

      render('templates/header', array('title' => 'C$75 Finance'));
      render('search_form', array('stocks' => $stocks));
      render('templates/footer');
    } else {
      render('templates/header', array('title' => 'C$75 Finance'));
      render('search_form', array('stocks' => null, 'error' => 'Stock symbol is required'));
      render('templates/footer');
    } 
    
  } else {
    // render search  
    render('templates/header', array('title' => 'C$75 Finance'));
    render('search_form', array('stocks' => null));
    render('templates/footer');
  }

} else {
  // render login page
  render('templates/header', array('title' => 'C$75 Finance'));
  render('login_form', array('error' => 'You must login first'));
  render('templates/footer');
}

?>
