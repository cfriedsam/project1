<?php

session_start();

//~ controller for selling stocks

require_once('../includes/helpers.php');
require_once('../model/user.php');

render('templates/header', array('title' => 'C$75 Finance'));

if (logged_in()) {
  // sell stocks, redirect to my portfolio
  if (isset($_POST['stocks'])) {
    $portfolio = current_user()->portfolio();
    if ($portfolio->sell($_POST['stocks'])) {
      flash('notice', 'Stocks have been sold successfully');
      header("Location: /");
    } else {
      flash('error', join(', ', $portfolio->errors()));
      render('dashboard');
    }
  } 
} else {
  // render login page
  render('login_form', array('error' => 'You must login first'));
}

render('templates/footer');

?>

