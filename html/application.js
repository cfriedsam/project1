var Finance = {
  // initialize callbacks based on elements on page
  init: function() {
    if ($('#search_results')) { Finance.init_buy() };
    if ($('#portfolio')) { Finance.init_sell() };
  },
  
  // bind costing updates to key events on search page
  init_buy: function() {
    $('#search_results .amount').bind('keyup', Finance.update_cost);
  },
  
  // bind amount updates to key events on portfolio page
  init_sell: function() {
    $('#portfolio .amount').bind('keyup', Finance.check_amount);
  },
  
  // parse price and quantity fields and update cost field
  // call update total to reflect changes in total purchase amount
  update_cost: function(e) {
    var e = e || window.event;
    var el = e.target;
    if (el.className == 'amount') {
      quantity = el.value != '' ? parseInt(el.value) : 0;
      value = parseFloat($(el).parents('tr').children('.value').text()).toFixed(2);
      cost = (value * quantity).toFixed(2);
      $(el).parents('tr').children('.cost').html('$' + cost);
      // disable submit and show error if quantity is negative
      if (quantity < 0) {
        alert('Sorry, but you can only enter positive numbers');
        $('#submit').attr('disabled', true);
        return false;
      }
    }
    Finance.update_total();
  },
  
  // iterate over all cost columns and update totals
  update_total: function() {
    sum = 0;
    $('#search_results .cost').each(function(index, el) {
      if ($(el).text() != '') {
        sum += parseFloat($(el).text().split('$')[1]);
      }
    });
    $('#total').html('$' + sum.toFixed(2));
    cash_balance = parseFloat($('#cash_balance').text().replace(/[\$,]/g,''));
    // display error and disable submit if not enough cash
    if (cash_balance < sum) {
      alert("Sorry, but you don't have enough cash");
      $('#submit').attr('disabled', 'disabled');
    } else { // re-enable submit if cash is sufficient
      $('#submit').removeAttr('disabled');
    }
  },
  
  // check if you hold enough stocks to sell specified quantity
  check_amount: function(e) {
    var e = e || window.event;
    var el = e.target;
    if (el.className == 'amount') {
      amount = el.value != '' ? parseInt(el.value) : 0;
      quantity = parseInt($(el).parents('tr').children('.quantity').text()).toFixed(0);
      // disable submit if amount > quantity
      if (amount > quantity) {
        alert('Sorry, but you can only sell ' + quantity + ' stocks');
        $('#submit').attr('disabled', true);
      } else if (amount < 0) { // disable submit if amount < 0
        alert('Sorry, but you can only enter positive numbers');
        $('#submit').attr('disabled', true);
      } else { // re-enable submit otherwise
        $('#submit').removeAttr('disabled');
      }
    }
  }
}

$(document).ready(Finance.init);
