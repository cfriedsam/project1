<?php

session_start();

//~ destroy cookie and session, reload page

setcookie(session_name(), "", time() - 3600);
session_destroy();

header("Location: /");
