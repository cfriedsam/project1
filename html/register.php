<?php

session_start();

//~ registration controller

require_once('../includes/helpers.php');
require_once('../model/user.php');

function render_view($user = null) {
  if (empty($user)) { $user = new User(); }
  render('templates/header', array('title' => 'C$75 Finance'));
  render('registration_form', array('user' => $user));
  render('templates/footer');
}

// Handle GET register, renders registration form
if (is_get_request()) {
  render_view();
}

// Handle POST register, creates new user and redirects
else if (is_post_request()) {
  // handle registration...
  
  $user = new User($_POST);
  // check if user is valid
  if ($user->valid()) {
    if ($user->save()) {
      set_current_user($user);
      
      header("Location: /welcome.php");
    } else {
      header("Location: /error.php");
    }
    
  } else {
    // user is invalid re-render form
    render_view($user);
  }
}

?>

