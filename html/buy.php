<?php

session_start();

//~ controller for buying stocks

require_once('../includes/helpers.php');
require_once('../model/user.php');

render('templates/header', array('title' => 'C$75 Finance'));

if (logged_in()) {
  // check cash balance, buy stocks, redirect to my portfolio
  if (isset($_POST['stocks'])) {
    $portfolio = current_user()->portfolio();
    if ($portfolio->buy($_POST['stocks'])) {
      flash('notice', 'Stocks have been bought successfully');
      header("Location: /");
    } else {
      render('search_form', array('stocks' => null, 'error' => join(', ', $portfolio->errors())));
    }
  } 
} else {
  // render login page
  render('login_form', array('error' => 'You must login first'));
}

render('templates/footer');

?>
