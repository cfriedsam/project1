<!--
welcome page after registration, checks for authentication
-->
<?php

session_start();

require_once('../includes/helpers.php');
require_once('../model/user.php');

if (logged_in()) {
  render('templates/header', array('title' => 'C$75 Finance'));
  render('welcome');
  render('templates/footer');

} else {
  render('templates/header', array('title' => 'C$75 Finance'));
  render('login_form', array('error' => 'You must login first'));
  render('templates/footer');
}

?>
