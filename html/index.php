<?php

session_start();

require_once('../includes/helpers.php');
require_once('../model/user.php');

render('templates/header', array('title' => 'C$75 Finance'));

if (logged_in()) {
  // render dashboard if user is authenticated
  render('dashboard');

} else if (isset($_POST['email'])) {
  // authenticate user
  if (($user = User::find_by_email($_POST['email'])) && $user->authenticated($_POST['password'])) {
    set_current_user($user);
    header("Location: /");
    
  } else {
    render('login_form', array('error' => 'Sorry, email or password incorrect'));
  }

} else {
  // render login page
  render('login_form');
}

render('templates/footer');

?>
