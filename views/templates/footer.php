<!--
Footer template
-->

    <footer>
      <div class="name">CS75Finance</div>
      <div class="details">
        <div id="author"> by: Claudia Friedsam</div>
        <div id="date"> last updated: July 25, 2012</div>
      </div>
    </footer>
  </body>
</html>
