<!DOCTYPE html>

<!--
Header template
-->

<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
    <title><?php echo htmlspecialchars($title) ?></title>
    <link type="text/css" href="styles.css" rel="stylesheet" />
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    <script type="text/javascript" src="application.js"></script>
  </head>
  <body>
    <header>
      <div class="user_information">
        <? if (isset($_SESSION['current_user_id'])) { ?>
          <a href="/logout.php">Logout</a>
        <? } ?>
      </div>
      <h1><a href="/"><?php echo htmlspecialchars ($title) ?></a></h1>
    </header>
</html>
