<!--
Navigation template, navigation appears if a user is authenticated
-->

<div id="navigation" class="cell">
  <? if (logged_in()) { ?>
    <ul>
      <? foreach (menus() as $label => $href) { ?>
        <li><a href="<?= $href ?>"><?= $label ?></a></li>
      <? } ?>
    </ul>
  <? } ?>
</div>

