<!--
This is the error page for the different forms
-->

<? if ($object->errors()) { ?>
  <div class="errors">
    <p>There are errors with this entry:</p>
    <ul>
      <? foreach ($object->errors() as $key => $values) { ?>
        <li><?= join(', ', $values) ?></li>
      <? } ?>
    </ul>
  </div>
<? } ?>
