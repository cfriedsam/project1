<!--
Start page after login. Displays portfolio for authenticated user
-->

<section>
  <div class="row">
    <?= render('templates/navigation'); ?>
    <div id="my-portfolio" class="cell">
      <h2>My Portfolio</h2>
      <?= flash_notification() ?>
      <?= flash_error() ?>
      <!-- This section is displayed if the portfolio has no stocks -->
      <? if (current_user()->portfolio()->is_empty()) { ?>
        <p>Your portfolio currently does not hold any stocks.</p>
        <p>Your current cash balance is: <?= current_user()->portfolio()->cash_balance() ?></p>
        <p>To buy some stocks for your portfolio, start by searching for something you'd like to buy.</p>
        <p><a href="/search.php" class="button">Search</a></p>
      
      <? } else { ?>
        <form action="sell.php" method="post" id="sell_form">
          <table id="portfolio" class="fixed">
            <thead>
              <tr>
                <th>Symbol</th>
                <th>Quantity</th>
                <th>Avg. / Share</th>
                <th>Investment</th>
                <th>Market</th>
                <th>+/-</th>
                <th>%</th>
                <th>Sell #</th>
              </tr>
            </thead>
            <tbody>
              <? foreach (current_user()->portfolio()->stocks() as $stock) { ?>
                <tr>
                  <td><?= $stock->symbol ?></td>
                  <td class="quantity"><?= $stock->quantity ?></td>
                  <td>$<?= number_format($stock->average_per_share(), 2) ?></td>
                  <td>$<?= number_format($stock->cost, 2) ?></td>
                  <td>$<?= number_format($stock->market_value(), 2) ?></td>
                  <td class="<?= market_indicator($stock) ?>"><?= number_format($stock->market_change(), 2) ?></td>
                  <td class="<?= market_indicator($stock) ?>"><?= number_format($stock->market_percent_change(), 2) ?></td>
                  <td><input type="text" name="stocks[<?= $stock->symbol ?>][amount]" class="amount" /></td>
                </tr>
              <? } ?>
            </tbody>
          </table>
        
          <p>Cash Balance: <?= current_user()->portfolio()->cash_balance() ?></p>
          <p>Current Market Value: <?= current_user()->portfolio()->market_value() ?></p>
          <p>Total Value: <?= current_user()->portfolio()->total_value() ?></p>
          
          <div class="actions">
            <input type="submit" value="Sell" id="submit" />
          </div>
        </form>
      <? } ?>
    </div>
  </div>
</section>
