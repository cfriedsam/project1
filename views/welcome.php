<!--
This is the welcome page which appears upon successful registration
-->

<section>
  <div class="row">
    <?= render('templates/navigation') ?>
    <div id="welcome" class="cell">
      <h1>Welcome</h1>
      
      <p>Congratulations, your registration was successful.</p>
      <p>You receive an initial gift of <strong>$10,000</strong>.</p>
      <p>Use it wise...</p>
      
      <a href="/" class="button">Continue</a>
    </div>
  </div>
  
</section>
