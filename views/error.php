<!--
General error page if something goes wrong
-->

<section>
  <div id="error">
    <h1>Sorry, something is wrong ...</h1>
    
    <p>The system was unable to complete your request.</p>
  </div>
</section>
