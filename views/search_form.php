<!--
This is the page for searching stock quotes.
-->

<section>
  <?= render('templates/navigation'); ?>
  <div id="search_form" class="cell">
    <h2>Search</h2>
    <!-- error checking -->
    <? if (isset($error)) { ?>
      <div class="error">
        <?= $error ?>
      </div>
    <? } ?>
    <form action="/search.php" method="get">
      <div class="group">
        <label for="symbol">Stock symbol</label>
        <input type="text" name="symbol" id="symbol" autofocus="true" placeholder="GOOG, MSFT" />
        <input type="submit" value="Search" />
      </div>
    </form>
    <!-- display results -->
    <?= render('search_results', array('stocks' => $stocks)); ?>
  </div>
</section>
