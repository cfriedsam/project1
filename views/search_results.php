<!--
Displays search results and provides the option to buy stocks.
-->

<!-- checks if stocks were found, if, displays results -->
<? if (isset($stocks) && !empty($stocks)) { ?>
  <p>Found some stocks matching your search: </p>
  
  <form action="/buy.php" method="post" id="buy_form">
    <table id="search_results" class="fixed">
      <thead>
        <tr>
          <th>Symbol</th>
          <th>$</th>
          <th>Date</th>
          <th>Time</th>
          <th>Change</th>
          <th>Avg</th>
          <th>Buy #</th>
          <th>Cost</th>
        </tr>
      </thead>
      <tbody>
        <? foreach ($stocks as $key => $values) { ?>
          <tr title="<?= "High: " . number_format(doubleval($values[6]), 2) ?>; <?= "Low: " . number_format(doubleval($values[7]), 2) ?>">
            <td><?= $values[0] ?></td>
            <td class="value">
              <input type="hidden" name="stocks[<?= htmlspecialchars($values[0]) ?>][price]" value="<?= number_format(doubleval($values[1]), 2) ?>" />
              <?= number_format(doubleval($values[1]), 2) ?>
            </td>
            <td><?= $values[2] ?></td>
            <td><?= $values[3] ?></td>
            <td><?= $values[4] ?></td>
            <td><?= number_format(doubleval($values[5]), 2) ?></td>
            <td><input type="text" class="amount" name="stocks[<?= htmlspecialchars($values[0]) ?>][amount]" /></td>
            <td class="cost">$0</td>
          </tr>
        <? } ?>
      </tbody>
    </table>
    <!-- display cash balance and total cost of selected stocks -->
    <p>Your cash balance is: <strong id="cash_balance"><?= current_user()->portfolio()->cash_balance() ?></strong></p>
    <p>Total cost: <strong id="total">$0</strong></p>
    <!-- buy button, only active if cash balance is sufficient -->
    <div class="actions">
      <input type="submit" value="Buy" id="submit" />
    </div>
  </form>
<!-- if no results were found -->
<? } else if (isset($stocks)) { ?>
  <p>Sorry, nothing could be found matching your input</p>
<? } ?>
