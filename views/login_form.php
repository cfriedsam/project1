<section>
<!--
Login screen, appears if user is not authenticated 
-->

 <div class="row">
    <?= render('templates/navigation'); ?>
    <div id="login" class="cell">
      <h2>Login</h2>
      <!-- error-checking for form entries-->
      <? if (isset($error)) { ?>
        <div class="error">
          <?= $error ?>
        </div>
      <? } ?>
      <form action="/" method="post">
        <div class="group">
          <label for="e-mail">E-Mail:</label>
          <input type="email" name="email" autofocus="true" />
        </div class="group">
        <div class="group">
          <label for="password">Password:</label>
          <input type="password" name="password" />
        </div>
        <div id="submit_actions">
          <input type="submit" value="Login" />
          <a href="register.php">Register</a>
      </div>
      </form>
    </div>
  </div>
</section>
