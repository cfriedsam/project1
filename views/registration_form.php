<!--
Registration form for new users
-->
<section>
  <div class="row">
    <?= render('templates/navigation'); ?>
    <div id="register" class="cell">
      <h2>Register</h2>
      <!-- display errors if there are any -->
      <?= render('templates/form_errors', array('object' => $user)); ?>
      <form action="register.php" method="post">
        <div class="group <?= has_error($user, 'email') ?>">
          <label for="e-mail">Please enter your e-Mail:</label>
          <input type="email" name="email" autofocus="true" value="<?= htmlspecialchars($user->email) ?>" />
        </div class="group">
        <div class="group <?= has_error($user, 'password') ?>">
          <label for="password">Please enter a password:</label>
          <input type="password" name="password" />
        </div>
        <div class="group <?= has_error($user, 'password') ?>">
          <label for="password">Repeat password:</label>
          <input type="password" name="password_confirmation" />
        </div>
        <div id="submit_actions">
          <input type="submit" value="Register" />
      </div>
      </form>
    </div>
  </div>
</section>
